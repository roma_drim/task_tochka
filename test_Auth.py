from selenium import webdriver
import unittest
import HtmlTestRunner
import time

class Test_auth(unittest.TestCase):

    def setUp(self):
        profile = webdriver.ChromeOptions()
        self.driver = webdriver.Chrome(options=profile)
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def test_auth(self):
        # Авторизация
        self.driver.get('https://enter.tochka.com/sandbox/v1/login/')
        self.driver.find_element_by_xpath('//*[@id="username"]').send_keys('sandbox')
        self.driver.find_element_by_xpath('//*[@id="password"]').send_keys('sandbox')

        self.driver.find_element_by_xpath(
            '/html/body/app-root/div/app-common-page/app-login-form/form/div[2]/div/div/app-tochka-button/button/span').click()
        # Вводим цифры из смс
        self.driver.find_element_by_xpath('//*[@id="code_input"]').send_keys('12345')
        time.sleep(1)
        # Проверяем успешна ли авторизация
        self.assertEqual(self.driver.current_url, 'https://enter.tochka.com/sandbox/v1/authorize/?response_type=code&client_id=sandbox')

    def test_wrong_code(self):
        self.driver.get('https://enter.tochka.com/sandbox/v1/login/')
        self.driver.find_element_by_xpath('//*[@id="username"]').send_keys('sandbox')
        self.driver.find_element_by_xpath('//*[@id="password"]').send_keys('sandbox')

        self.driver.find_element_by_xpath(
            '/html/body/app-root/div/app-common-page/app-login-form/form/div[2]/div/div/app-tochka-button/button/span').click()
        # Вводим неправильные цифры из смс
        self.driver.find_element_by_xpath('//*[@id="code_input"]').send_keys('54321')
        time.sleep(1)
        self.assertEqual(self.driver.find_element_by_xpath('/html/body/app-root/div/app-common-page/app-modal-page'
                                                           '/div/div[2]/app-code-form/div[2]/div/div[2]').text,
                                                           'Неверный смс‑код. Исправьте его или отправьте себе ещё один.')
        # Кликаем "Отправить код еще раз"
        self.driver.find_element_by_xpath('/html/body/app-root/div/app-common-page/app-modal-page/div/div[2]/app-code-form/div[2]/div/div[3]/a').click()
        # Вводим правильные цифры из смс
        self.driver.find_element_by_xpath('//*[@id="code_input"]').send_keys('12345')
        self.assertEqual(self.driver.current_url,
                         'https://enter.tochka.com/sandbox/v1/authorize/?response_type=code&client_id=sandbox')

    def test_bad_auth(self):
        self.driver.get('https://enter.tochka.com/sandbox/v1/login/')
        # Неправильный пароль
        self.driver.find_element_by_xpath('//*[@id="username"]').send_keys('sandbox')
        self.driver.find_element_by_xpath('//*[@id="password"]').send_keys('sandboxxx')
        self.driver.find_element_by_xpath(
            '/html/body/app-root/div/app-common-page/app-login-form/form/div[2]/div/div/app-tochka-button/button/span').click()
        self.assertEqual(self.driver.find_element_by_xpath('/html/body/app-root/div/app-common-page/app-login-form'
                                                           '/form/div[1]/div/div[2]/div/div/div').text, 'Неверный логин или пароль. Попробуйте ещё раз или восстановите пароль.')
        self.driver.refresh()
        # Неправильный логин
        self.driver.find_element_by_xpath('//*[@id="username"]').send_keys('ssandboxx1')
        self.driver.find_element_by_xpath('//*[@id="password"]').send_keys('sandbox')
        self.driver.find_element_by_xpath(
            '/html/body/app-root/div/app-common-page/app-login-form/form/div[2]/div/div/app-tochka-button/button/span').click()
        self.assertEqual(self.driver.find_element_by_xpath('/html/body/app-root/div/app-common-page/app-login-form'
                                                           '/form/div[1]/div/div[2]/div/div/div').text,
                         'Неверный логин или пароль. Попробуйте ещё раз или восстановите пароль.')
        self.driver.refresh()
        # Изменение регистра
        self.driver.find_element_by_xpath('//*[@id="username"]').send_keys('Sandbox')
        self.driver.find_element_by_xpath('//*[@id="password"]').send_keys('sandbox')
        self.driver.find_element_by_xpath(
            '/html/body/app-root/div/app-common-page/app-login-form/form/div[2]/div/div/app-tochka-button/button/span').click()
        self.assertEqual(self.driver.find_element_by_xpath('/html/body/app-root/div/app-common-page/app-login-form'
                                                           '/form/div[1]/div/div[2]/div/div/div').text,
                         'Неверный логин или пароль. Попробуйте ещё раз или восстановите пароль.')

    def test_show_pass(self):
        self.driver.get('https://enter.tochka.com/sandbox/v1/login/')
        self.driver.find_element_by_xpath('//*[@id="password"]').send_keys('sandbox')
        # Проверяем виден ли введенный пароль
        self.assertEqual(self.driver.find_element_by_xpath('//*[@id="password"]').get_attribute("type"), 'password')
        # Делаем пароль видимым
        self.driver.find_element_by_xpath(
            '/html/body/app-root/div/app-common-page/app-login-form/form/div[1]/div/div['
            '2]/div/fieldset/app-tochka-input[2]/div/div/div').click()
        # Проверяем виден ли пароль
        self.assertEqual(self.driver.find_element_by_xpath('//*[@id="password"]').get_attribute("type"), 'text')

    def test_close_button(self):
        self.driver.get('https://enter.tochka.com/sandbox/v1/login/')
        self.driver.find_element_by_xpath('//*[@id="username"]').send_keys('sandbox')
        self.driver.find_element_by_xpath('//*[@id="password"]').send_keys('sandbox')

        self.driver.find_element_by_xpath(
            '/html/body/app-root/div/app-common-page/app-login-form/form/div[2]/div/div/app-tochka-button/button/span').click()
        # Не вводим цифры из смс, а нажимаем на Close-Button"
        time.sleep(1)
        self.driver.find_element_by_xpath('//*[@id="Close-Button"]').click()
        time.sleep(1)
        self.assertEqual(self.driver.find_element_by_xpath('/html/body/app-root/div/app-common-page/app-login-form/form/div[1]/div/div[1]').text, 'Вход в интернет‑банк')

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output='reports'))
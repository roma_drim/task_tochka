## Test task Tochka

## Запуск

#### Virtualenv
```shell script
$ python3 -m pip install virtualenv
$ cd /path/to/project
$ python3 -m virtualenv venv
$ source venv/bin/activate
```
### Chromedriver
Скачать chromedriver отсюда: https://chromedriver.chromium.org/
Чтобы WebDriver обнаружил chromedriver, нужно выполнить один из двух вариантов:
* включить местоположение ChromeDriver в  переменную  среды PATH
* включить путь к ChromeDriver при создании экземпляра  webdriver.Chrome (Добавть путь вторым аргументом test_smartech/test_task.py line 11)

#### Установка зависимостей
```shell script
(venv) $ pip install -r requirements.txt 
```
#### Запуск
```shell script
(venv) $ python3 test_access.py
(venv) $ python3 test_Auth.py
```


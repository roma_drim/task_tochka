from selenium import webdriver
import unittest
import HtmlTestRunner
import time
import re


class Test_access(unittest.TestCase):

    def setUp(self):
        profile = webdriver.ChromeOptions()
        self.driver = webdriver.Chrome(options=profile)
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()
        self.driver.get('https://enter.tochka.com/sandbox/v1/login/')
        self.driver.find_element_by_xpath('//*[@id="username"]').send_keys('sandbox')
        self.driver.find_element_by_xpath('//*[@id="password"]').send_keys('sandbox')
        self.assertEqual(self.driver.find_element_by_xpath('//*[@id="password"]').get_attribute("type"), 'password')
        self.driver.find_element_by_xpath('/html/body/app-root/div/app-common-page/app-login-form/form/div[1]/div/div[2]/div/fieldset/app-tochka-input[2]/div/div/div').click()
        time.sleep(3)
        self.assertEqual(self.driver.find_element_by_xpath('//*[@id="password"]').get_attribute("type"), 'text')
        self.driver.find_element_by_xpath(
            '/html/body/app-root/div/app-common-page/app-login-form/form/div[2]/div/div/app-tochka-button/button/span').click()
        self.driver.find_element_by_xpath('//*[@id="code_input"]').send_keys('12345')

    def test_no_chose(self):
        self.driver.get('https://enter.tochka.com/sandbox/v1/authorize/?response_type=code&client_id=sandbox')
        self.driver.find_element_by_xpath('/html/body/main/form/button').submit()
        self.assertEqual(self.driver.current_url, 'http://localhost:8000/')

    def test_1_chose(self):
        self.driver.get('https://enter.tochka.com/sandbox/v1/authorize/?response_type=code&client_id=sandbox')
        for i in range(1, 3):
            self.driver.find_element_by_xpath(f'/html/body/main/form/label[{i}]/span/span[1]').click()
            self.driver.find_element_by_xpath('/html/body/main/form/button').submit()
            self.assertTrue(re.match(r'^http:\/\/localhost:8000\/\?code=[a-zA-Z0-9]{32}$', self.driver.current_url))
            self.driver.back()
            self.driver.refresh()

    def test_2_chose(self):
        self.driver.get('https://enter.tochka.com/sandbox/v1/authorize/?response_type=code&client_id=sandbox')
        for i in range(1, 3):
            if(i == 3):
                self.driver.find_element_by_xpath(f'/html/body/main/form/label[{i}]/span/span[1]').click()
                self.driver.find_element_by_xpath(f'/html/body/main/form/label[1]/span/span[1]').click()
                self.driver.find_element_by_xpath('/html/body/main/form/button').submit()
                self.assertTrue(re.match(r'^http:\/\/localhost:8000\/\?code=[a-zA-Z0-9]{32}$', self.driver.current_url))
                self.driver.back()
                self.driver.refresh()
            self.driver.find_element_by_xpath(f'/html/body/main/form/label[{i}]/span/span[1]').click()
            self.driver.find_element_by_xpath(f'/html/body/main/form/label[{i+1}]/span/span[1]').click()
            self.driver.find_element_by_xpath('/html/body/main/form/button').submit()
            self.assertTrue(re.match(r'^http:\/\/localhost:8000\/\?code=[a-zA-Z0-9]{32}$', self.driver.current_url))
            self.driver.back()
            self.driver.refresh()

    def test_3_chose(self):
        self.driver.get('https://enter.tochka.com/sandbox/v1/authorize/?response_type=code&client_id=sandbox')
        self.driver.find_element_by_xpath('/html/body/main/form/label[1]/span/span[1]').click()
        self.driver.find_element_by_xpath('/html/body/main/form/label[2]/span/span[1]').click()
        self.driver.find_element_by_xpath('/html/body/main/form/label[3]/span/span[1]').click()
        self.driver.find_element_by_xpath('/html/body/main/form/button').submit()
        self.assertTrue(re.match(r'^http:\/\/localhost:8000\/\?code=[a-zA-Z0-9]{32}$', self.driver.current_url))

    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output='reports'))
